// init audio context
window.AudioContext = window.AudioContext || window.webkitAudioContext;
const context = new AudioContext();

// max track per project
const MAX_TRACKS = 8;

// max beats per track
const MAX_BEATS = 32;

// default track object
const DEFAULT_TRACK = {
    id: 0,
    title: null,
    sample: null,
    pattern: {},
    volume: 100,
};

const tempo = 140; // BPM (beats per minute)
const eighthNoteTime = (60 / tempo) / 2;

// project state
const state = {
    title: 'Untitled Project',
    tracks: [Object.assign({}, DEFAULT_TRACK, { pattern: {} })],
    playing: false,
};

// dom elements render functions
const render = {
    header: title => {
        const elHeader = document.createElement('h1');

        elHeader.className = 'header';
        elHeader.textContent = title;
        elHeader.title = title;
        elHeader.appendChild(render.resetButton());

        return elHeader;
    },

    resetButton: () => {
        const elButton = document.createElement('button');

        elButton.className = 'reset-project-button';
        elButton.textContent = 'RESET PROJECT';

        return elButton;
    },

    beat: (selected, trackId, beatId) => {
        const elBeat = document.createElement('input');

        elBeat.type = 'checkbox';
        elBeat.className = 'beat';
        elBeat.title = 'Click to select';
        elBeat.checked = selected;
        elBeat.dataset.trackId = trackId;
        elBeat.dataset.beatId = beatId;

        return elBeat;
    },

    beats: (pattern, trackId) => {
        let elBeats = document.createElement('div');

        elBeats.className = 'beats';

        for (let i = 0; i < MAX_BEATS; i++) {
            elBeats.appendChild(render.beat(pattern[i], trackId, i));
        }

        return elBeats;
    },

    loadButton: (title, trackId) => {
        const elButton = document.createElement('button');

        elButton.className = 'load-sample-button';
        elButton.autofocus = true;
        elButton.textContent = title || 'LOAD SAMPLE';
        elButton.title = title || 'LOAD SAMPLE';
        elButton.dataset.trackId = trackId;

        return elButton;
    },

    volume: (value, trackId) => {
        const elVolume = document.createElement('input');

        elVolume.type = 'range';
        elVolume.className = 'track-volume';
        elVolume.title = 'Change track volume level';
        elVolume.min = 0;
        elVolume.max = 100;
        elVolume.value = value;
        elVolume.dataset.trackId = trackId;

        return elVolume;
    },

    track: ({ id, title, pattern, volume }) => {
        const elTrack = document.createElement('li');

        elTrack.className = 'track';
        elTrack.dataset.trackId = id;
        elTrack.appendChild(render.loadButton(title, id));
        elTrack.appendChild(render.volume(volume, id));
        elTrack.appendChild(render.beats(pattern, id));

        return elTrack;
    },

    tracks: tracks => {
        const elTracks = document.createDocumentFragment();

        tracks.forEach(track => elTracks.appendChild(render.track(track)));

        return elTracks;
    },
};

// get track object by its ID
const getTrackById = id => state.tracks.find(track => track.id === id);

// read file as array buffer
const readFile = file => new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.addEventListener('load', ({ target: { result } }) => resolve(result));
    reader.addEventListener('error', reject);

    reader.readAsArrayBuffer(file);
});

// handle file select by user
const handleFileSelect = (files, trackId) => {
    if (!files.length) {
        return;
    }

    readFile(files[0]).then(data => context.decodeAudioData(data, audioBuffer => {
        getTrackById(trackId).sample = audioBuffer;
        playSound(audioBuffer);
    }));
};

// play sound buffer
const playSound = (buffer, volume = 100, time = 0) => {
    if (!buffer) {
        return;
    }

    const source = context.createBufferSource();
    const gainNode = context.createGain();

    const fraction = parseInt(volume) / 100;

    source.buffer = buffer;
    source.connect(gainNode);

    gainNode.connect(context.destination);
    gainNode.gain.value = fraction * fraction;

    source.start(time);
};

// reset project
const resetProject = () => {
    state.tracks = [DEFAULT_TRACK];

    elTracksList.innerHTML = '';
    elTracksList.appendChild(render.tracks(state.tracks));
};

// play project
const playProject = () => {
    const startTime = context.currentTime + .1;

    // Play 2 bars of the following:
    for (let bar = 0; bar < 1; bar++) {
        const time = startTime + bar * 8 * eighthNoteTime;

        state.tracks.forEach(({ sample, pattern, volume }) => {
            const beats = Object.entries(pattern).filter(([num, selected]) => selected).map(([num]) => Number(num));

            beats.forEach(beat => playSound(sample, volume, time + beat * eighthNoteTime));
        });
    }

    const intervalTime = eighthNoteTime * 1000;
    let beatsPassed = 0;

    const highlightTracks = () => {
        elPanel.querySelectorAll('.beats__current').forEach(elBeats => elBeats.classList.remove('beats__current'));

        state.tracks.forEach(({ id, pattern, volume }) => {
            if (!volume) {
                return;
            }

            const beats = Object.entries(pattern).filter(([beat, selected]) => selected).map(([beat]) => Number(beat));

            if (beats.indexOf(beatsPassed) > -1) {
                elPanel.querySelector(`.track[data-track-id="${id}"] .beats`).classList.add('beats__current');
            }
        });
    };

    const moveBeatIndicator = () => {
        const prevBeats = elTracksList.querySelectorAll(`.beats .beat:nth-child(${beatsPassed})`);
        const nextBeats = elTracksList.querySelectorAll(`.beats .beat:nth-child(${beatsPassed + 1})`);

        if (prevBeats) {
            prevBeats.forEach(elBeat => elBeat.classList.remove('beat__current'));
        }
        if (nextBeats) {
            nextBeats.forEach(elBeat => elBeat.classList.add('beat__current'));
        }
    };

    const interval = setInterval(() => {
        moveBeatIndicator();
        highlightTracks();

        beatsPassed++;

        if (beatsPassed === MAX_BEATS + 1) {
            clearInterval(interval);
            // playProject();
        }
    }, intervalTime);

    moveBeatIndicator();
};

// dom elements
const elSampleChooser = document.querySelector('.sample-chooser-input');
const elPanel = document.querySelector('.panel');
const elTracksList = elPanel.querySelector('.tracks');

// global click event listener
elPanel.addEventListener('click', ({ target }) => {
    const { classList, dataset: { trackId, beatId }, checked } = target;

    // handle reset button click
    if (classList.contains('reset-project-button')) {
        resetProject();
    }

    // handle load sample button click
    if (classList.contains('load-sample-button')) {
        const handler = ({ target: { files } }) => {
            elSampleChooser.removeEventListener('change', handler);
            handleFileSelect(files, Number(trackId));
            target.textContent = files[0].name;
            target.title = files[0].name;
        };

        elSampleChooser.addEventListener('change', handler);
        elSampleChooser.click();
    }

    // handle add track button click
    if (classList.contains('add-track-button')) {
        if (state.tracks.length >= MAX_TRACKS) {
            return;
        }

        const track = Object.assign({}, DEFAULT_TRACK, {
            id: state.tracks.length,
            pattern: [],
        },);
        const elTrack = render.track(track);

        state.tracks.push(track);
        elTracksList.appendChild(elTrack);
    }

    // Handle play project button click
    if (classList.contains('play-button')) {
        playProject();
    }

    // handle beat cell selector click
    if (classList.contains('beat')) {
        const track = getTrackById(Number(trackId));

        track.pattern[Number(beatId)] = checked;

        if (checked) {
            playSound(track.sample);
        }
    }
});

// global change event listener
elPanel.addEventListener('change', ({ target }) => {
    const { classList, dataset: { trackId }, value } = target;

    if (classList.contains('track-volume')) {
        const track = getTrackById(Number(trackId));

        track.volume = Number(value);

        const elTrack = elPanel.querySelector(`.track[data-track-id="${trackId}"]`);
        elTrack.classList.toggle('track__muted', !Boolean(Number(value)));
    }
});

// init project (render header and default tracks)
elPanel.insertBefore(render.header(state.title), elTracksList);
elTracksList.appendChild(render.tracks(state.tracks));
